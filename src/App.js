import React from 'react';
import './App.scss';
import Posts from "./components/Posts";
import Comments from "./components/Comments";
import Photos from "./components/Photos";

function App() {
    return (
        <div className="App">
            <div className="wrapper">
                <Posts/>
                <Comments/>
                <Photos/>
            </div>
        </div>
    );
}

export default App;
