import React, {useState, useEffect} from "react";
import Loading from './../Loading';
import './style.scss';

const Comments = () => {

    const [comments, setComments] = useState([]);

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/comments')
            .then(response => response.json())
            .then(result => setComments(result));
    }, []);


    return (
        <div className="comments">
            <p className="comments__title">Comments</p>
            <div className="comments__items">
                {comments.length > 0 ? comments.map((comment) => {
                    return <div className="comments__item" key={comment.id}>
                        <p className="comments__item-title">{comment.name}</p>
                        <a href={comment.email} className="comments__item-pre">{comment.email}</a>
                        <p className="comments__item-body">{comment.body}</p>
                    </div>
                }) : <Loading/>}
            </div>
        </div>
    )
};

export default Comments;