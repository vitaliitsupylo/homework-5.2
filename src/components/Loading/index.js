import React from "react";

const Loading = () => {
    return(
        <img src="/load.gif" alt="" style={{width:'250px', height: 'auto', display: 'block', margin: 'auto'}}/>
    )
};

export default Loading;
