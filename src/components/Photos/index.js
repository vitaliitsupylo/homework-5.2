import React, {useState, useEffect} from "react";
import Loading from './../Loading';
import './style.scss';

const Photos = () => {

    const [photos, setPhotos] = useState([]);

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/photos')
            .then(response => response.json())
            .then(result => setPhotos(result));
    }, []);


    return (
        <div className="photos">
            <p className="photos__title">Photos</p>
            <div className="photos__items">
                {photos.length > 0 ? photos.map((photo) => {
                    return <div className="photos__item" key={photo.id}>
                        <p className="photos__item-title">{photo.title}</p>
                        <img className="photos__item-photo" src={photo.thumbnailUrl} alt=""/>
                    </div>
                }) : <Loading/>}
            </div>
        </div>
    )
};

export default Photos;