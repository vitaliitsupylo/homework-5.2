import React, {useState, useEffect} from "react";
import Loading from './../Loading';
import './style.scss';

const Posts = () => {

    const [posts, setPosts] = useState([]);

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(result => setPosts(result));
    }, []);


    return (
        <div className="posts">
            <p className="posts__title">Posts</p>
            <div className="posts__items">
                {posts.length > 0 ? posts.map((post) => {
                    return <div className="posts__item" key={post.id}>
                        <p className="posts__item-title">{post.title}</p>
                        <p className="posts__item-body">{post.body}</p>
                    </div>
                }) : <Loading/>}
            </div>
        </div>
    )
};

export default Posts;